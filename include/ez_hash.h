
#ifndef __EZ_HASH_H
#define __EZ_HASH_H

uint32_t ez_hash_jenkins(const char *key, size_t length);

#endif				/* __EZ_HASH_H */
